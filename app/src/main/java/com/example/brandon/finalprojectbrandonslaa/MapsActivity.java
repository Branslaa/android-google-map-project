package com.example.brandon.finalprojectbrandonslaa;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener {
	
	private GoogleMap mMap;
	private static final int PERMISSION_ACCESS_FINE_LOCATION = 1;
	private GoogleApiClient googleApiClient;
	private LatLng myLoc;
	private LatLng parkLoc;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_maps);
		
		SharedPreferences pref = getSharedPreferences("content", Context.MODE_PRIVATE);
		parkLoc = new LatLng(pref.getFloat("lat", 0.0f), pref.getFloat("long", 0.0f));
		
		if (ContextCompat.checkSelfPermission(this,
				android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this,
					new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
		}
		
		// Obtain the SupportMapFragment and get notified when the map is ready to be used.
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);
		
		// set up the googleApiClient
		googleApiClient = new GoogleApiClient.Builder
				(this, this, this).addApi(LocationServices.API).build();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		if (googleApiClient != null) {
			googleApiClient.connect();}
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		googleApiClient.disconnect();
	}
	
	@Override
	public void onConnectionSuspended(int i) {
		Log.i(MapsActivity.class.getSimpleName(),
				"Connection suspended to Google Play Services!");
	}
	
	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
		Log.i(MapsActivity.class.getSimpleName(),
				"Can't connect to Google Play Services!");
	}
	
	
	@Override
	public void onConnected(@Nullable Bundle bundle) {
		Log.i(MapsActivity.class.getSimpleName(), "Connected to Google Play Services!");
		
		if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
				== PackageManager.PERMISSION_GRANTED) {
			Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
			
			double lat = lastLocation.getLatitude(), lon = lastLocation.getLongitude();
			myLoc = new LatLng(lat, lon);
			Log.i("Location", myLoc.toString());
			
			markStart();
			geo();
		}
	}
	
	private void markStart() {
		Geocoder geocoder = new Geocoder(this, Locale.getDefault());
		List<Address> addresses = null;
		
		try {
			addresses = geocoder.getFromLocation(myLoc.latitude, myLoc.longitude, 1);
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (IllegalArgumentException e2) {
			// Error message to post in the log
			e2.printStackTrace();
		}
		// If the reverse geocode returned an address
		if (addresses != null && addresses.size() > 0) {
			// Get the first address
			Address address = addresses.get(0);
			
			String addressText = String.format(
					"%s%s, %s",
					// If there's a street address, add it
					address.getMaxAddressLineIndex() > 0 ? address
							.getAddressLine(0) + ", " : "",
					// Locality is usually a city
					address.getLocality(),
					// The country of the address
					address.getCountryName());
			mMap.addMarker(new MarkerOptions().position(myLoc).title(addressText).snippet("You are Here"));
			
		}
	}
	
	@Override
	public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;
	}
	
	private void geo() {
		Geocoder geocoder = new Geocoder(this, Locale.getDefault());
		List<Address> addresses = null;
		
		try {
			addresses = geocoder.getFromLocation(parkLoc.latitude, parkLoc.longitude, 1);
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (IllegalArgumentException e2) {
			// Error message to post in the log
			e2.printStackTrace();
		}
		// If the reverse geocode returned an address
		if (addresses != null && addresses.size() > 0) {
			// Get the first address
			Address address = addresses.get(0);
			
			SharedPreferences pref = getSharedPreferences("content", Context.MODE_PRIVATE);
			String parkName = pref.getString("park", "N/A");
			
			String addressText = String.format(
					"%s, %s",
					parkName,
					// The country of the address
					address.getCountryName());
			
			
			float[] distance = new float[1];
			Location.distanceBetween(myLoc.latitude, myLoc.longitude, parkLoc.latitude, parkLoc.longitude, distance);
			NumberFormat nf_us = NumberFormat.getInstance(Locale.US);
			String dInKM = nf_us.format((int)distance[0]/1000 );
			
			String distanceBetween = String.format(
					"You need to travel %s km to reach this destination", dInKM);
			
			mMap.addMarker(new MarkerOptions().position(parkLoc).title(addressText).snippet(distanceBetween));
			mMap.moveCamera(CameraUpdateFactory.newLatLng(parkLoc));
			mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(parkLoc, 5));
			mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		}
	}
}
