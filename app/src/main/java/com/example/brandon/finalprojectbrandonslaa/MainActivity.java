package com.example.brandon.finalprojectbrandonslaa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

public class MainActivity extends AppCompatActivity {
	
	CardView cv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		cv = (CardView) findViewById(R.id.cardAbout);
	}
	
	public void toggleAboutCard(View view) {
		if (cv.getVisibility() == View.VISIBLE)
			cv.setVisibility(View.INVISIBLE);
		else
			cv.setVisibility(View.VISIBLE);
	}
	
	public void goToTerrain(View view) {
		Intent intent = new Intent(MainActivity.this, terrainActivity.class);
		startActivity(intent);
	}
}
