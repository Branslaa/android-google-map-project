package com.example.brandon.finalprojectbrandonslaa;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class terrainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
	
	ListView lv;
	String terrainChoice = "none";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_terrain);
		
		lv = (ListView)findViewById(R.id.terrainLV);
		lv.setOnItemClickListener(this);
	}
	
	public void terrainNext(View view) {
		if (!terrainChoice.equals("none")) {
			Intent intent = new Intent(terrainActivity.this, temperatureActivity.class);
			startActivity(intent);
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
		ConstraintLayout layout = (ConstraintLayout)findViewById(R.id.terrainLayout);
		terrainChoice = ((TextView)view).getText().toString();
		
		switch (terrainChoice) {
			case "Forest":
				layout.setBackgroundResource(R.drawable.forest);
				break;
			case "Mountains":
				layout.setBackgroundResource(R.drawable.mountain);
				break;
			case "Plains":
				layout.setBackgroundResource(R.drawable.plain);
				break;
			case "Desert":
				layout.setBackgroundResource(R.drawable.desert);
				break;
			case "Beach":
				layout.setBackgroundResource(R.drawable.beach);
				break;
			default:
				break;
		}
		
		SharedPreferences pref = getSharedPreferences("content", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString("terrainChoice", terrainChoice);
		editor.apply();
		
	}
}
