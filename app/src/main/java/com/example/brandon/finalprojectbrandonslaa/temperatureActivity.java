package com.example.brandon.finalprojectbrandonslaa;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class temperatureActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
	
	ListView lv;
	String temperatureChoice = "none";
	SharedPreferences pref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_temperature);
		
		pref = getSharedPreferences("content", Context.MODE_PRIVATE);
		
		lv = (ListView)findViewById(R.id.temperatureLV);
		lv.setOnItemClickListener(this);
	}
	
	public void temperatureNext(View view) {
		if (!temperatureChoice.equals("none")) {
			Intent intent = new Intent(temperatureActivity.this, resortActivity.class);
			startActivity(intent);
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
		ConstraintLayout layout = (ConstraintLayout)findViewById(R.id.temperatureLayout);
		temperatureChoice = ((TextView)view).getText().toString();
		
		switch (temperatureChoice) {
			case "Cold":
				layout.setBackgroundResource(R.drawable.cold);
				break;
			case "Mild":
				layout.setBackgroundResource(R.drawable.mild);
				break;
			case "Hot":
				layout.setBackgroundResource(R.drawable.hot);
				break;
			default:
				break;
		}
		
		SharedPreferences pref = getSharedPreferences("content", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString("temperatureChoice", temperatureChoice);
		editor.apply();
	}
}
