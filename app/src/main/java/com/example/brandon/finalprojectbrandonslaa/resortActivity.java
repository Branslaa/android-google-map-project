package com.example.brandon.finalprojectbrandonslaa;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class resortActivity extends AppCompatActivity {
	
	TextView tvName;
	TextView tvCountry;
	TextView tvSnippet;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_resort);
		
		tvName = (TextView)findViewById(R.id.txtTerrainPrompt);
		tvCountry = (TextView)findViewById(R.id.txtParkCountry);
		tvSnippet = (TextView)findViewById(R.id.txtParkSnippet);
		
		SharedPreferences pref = getSharedPreferences("content", Context.MODE_PRIVATE);
		String terrain = pref.getString("terrainChoice", "none");
		String temperature = pref.getString("temperatureChoice", "none");
		
		fillTextViews(terrain, temperature);
	}
	
	public void openMap(View view) {
		Intent intent = new Intent(resortActivity.this, MapsActivity.class);
		startActivity(intent);
	}
	
	public void backToStart(View view) {
		Intent intent = new Intent(resortActivity.this, MainActivity.class);
		startActivity(intent);
	}
	
	public void fillTextViews(String terrain, String temperature) {
		SharedPreferences pref = getSharedPreferences("content", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		
		if (terrain.equals("Forest") && temperature.equals("Cold")) {
			tvName.setText(getString(R.string.coldForestName));
			tvCountry.setText(getString(R.string.coldForestCountry));
			tvSnippet.setText(getString(R.string.coldForestSnippet));
			editor.putFloat("lat", Float.parseFloat(getString(R.string.coldForestLat)));
			editor.putFloat("long", Float.parseFloat(getString(R.string.coldForestLong)));
			editor.putString("park", getString(R.string.coldForestName));
		}
		else if (terrain.equals("Forest") && temperature.equals("Mild")) {
			tvName.setText(getString(R.string.mildForestName));
			tvCountry.setText(getString(R.string.mildForestCountry));
			tvSnippet.setText(getString(R.string.mildForestSnippet));
			editor.putFloat("lat", Float.parseFloat(getString(R.string.mildForestLat)));
			editor.putFloat("long", Float.parseFloat(getString(R.string.mildForestLong)));
			editor.putString("park", getString(R.string.mildForestName));
		}
		else if (terrain.equals("Forest") && temperature.equals("Hot")) {
			tvName.setText(getString(R.string.hotForestName));
			tvCountry.setText(getString(R.string.hotForestCountry));
			tvSnippet.setText(getString(R.string.hotForestSnippet));
			editor.putFloat("lat", Float.parseFloat(getString(R.string.hotForestLat)));
			editor.putFloat("long", Float.parseFloat(getString(R.string.hotForestLong)));
			editor.putString("park", getString(R.string.hotForestName));
		}
		else if (terrain.equals("Mountains") && temperature.equals("Cold")) {
			tvName.setText(getString(R.string.coldMountainName));
			tvCountry.setText(getString(R.string.coldMountainCountry));
			tvSnippet.setText(getString(R.string.coldMountainSnippet));
			editor.putFloat("lat", Float.parseFloat(getString(R.string.coldMountainLat)));
			editor.putFloat("long", Float.parseFloat(getString(R.string.coldMountainLong)));
			editor.putString("park", getString(R.string.coldMountainName));
		}
		else if (terrain.equals("Mountains") && temperature.equals("Mild")) {
			tvName.setText(getString(R.string.mildMountainName));
			tvCountry.setText(getString(R.string.mildMountainCountry));
			tvSnippet.setText(getString(R.string.mildMountainSnippet));
			editor.putFloat("lat", Float.parseFloat(getString(R.string.mildMountainLat)));
			editor.putFloat("long", Float.parseFloat(getString(R.string.mildMountainLong)));
			editor.putString("park", getString(R.string.mildMountainName));
		}
		else if (terrain.equals("Mountains") && temperature.equals("Hot")) {
			tvName.setText(getString(R.string.hotMountainName));
			tvCountry.setText(getString(R.string.hotMountainCountry));
			tvSnippet.setText(getString(R.string.hotMountainSnippet));
			editor.putFloat("lat", Float.parseFloat(getString(R.string.hotMountainLat)));
			editor.putFloat("long", Float.parseFloat(getString(R.string.hotMountainLong)));
			editor.putString("park", getString(R.string.hotMountainName));
		}
		else if (terrain.equals("Plains") && temperature.equals("Cold")) {
			tvName.setText(getString(R.string.coldPlainName));
			tvCountry.setText(getString(R.string.coldPlainCountry));
			tvSnippet.setText(getString(R.string.coldPlainSnippet));
			editor.putFloat("lat", Float.parseFloat(getString(R.string.coldPlainLat)));
			editor.putFloat("long", Float.parseFloat(getString(R.string.coldPlainLong)));
			editor.putString("park", getString(R.string.coldPlainName));
		}
		else if (terrain.equals("Plains") && temperature.equals("Mild")) {
			tvName.setText(getString(R.string.mildPlainName));
			tvCountry.setText(getString(R.string.mildPlainCountry));
			tvSnippet.setText(getString(R.string.mildPlainSnippet));
			editor.putFloat("lat", Float.parseFloat(getString(R.string.mildPlainLat)));
			editor.putFloat("long", Float.parseFloat(getString(R.string.mildPlainLong)));
			editor.putString("park", getString(R.string.mildPlainName));
		}
		else if (terrain.equals("Plains") && temperature.equals("Hot")) {
			tvName.setText(getString(R.string.hotPlainName));
			tvCountry.setText(getString(R.string.hotPlainCountry));
			tvSnippet.setText(getString(R.string.hotPlainSnippet));
			editor.putFloat("lat", Float.parseFloat(getString(R.string.hotPlainLat)));
			editor.putFloat("long", Float.parseFloat(getString(R.string.hotPlainLong)));
			editor.putString("park", getString(R.string.hotPlainName));
		}
		else if (terrain.equals("Desert") && temperature.equals("Cold")) {
			tvName.setText(getString(R.string.coldDesertName));
			tvCountry.setText(getString(R.string.coldDesertCountry));
			tvSnippet.setText(getString(R.string.coldDesertSnippet));
			editor.putFloat("lat", Float.parseFloat(getString(R.string.coldDesertLat)));
			editor.putFloat("long", Float.parseFloat(getString(R.string.coldDesertLong)));
			editor.putString("park", getString(R.string.coldDesertName));
		}
		else if (terrain.equals("Desert") && temperature.equals("Mild")) {
			tvName.setText(getString(R.string.mildDesertName));
			tvCountry.setText(getString(R.string.mildDesertCountry));
			tvSnippet.setText(getString(R.string.mildDesertSnippet));
			editor.putFloat("lat", Float.parseFloat(getString(R.string.mildDesertLat)));
			editor.putFloat("long", Float.parseFloat(getString(R.string.mildDesertLong)));
			editor.putString("park", getString(R.string.mildDesertName));
		}
		else if (terrain.equals("Desert") && temperature.equals("Hot")) {
			tvName.setText(getString(R.string.hotDesertName));
			tvCountry.setText(getString(R.string.hotDesertCountry));
			tvSnippet.setText(getString(R.string.hotDesertSnippet));
			editor.putFloat("lat", Float.parseFloat(getString(R.string.hotDesertLat)));
			editor.putFloat("long", Float.parseFloat(getString(R.string.hotDesertLong)));
			editor.putString("park", getString(R.string.hotDesertName));
		}
		else if (terrain.equals("Beach") && temperature.equals("Cold")) {
			tvName.setText(getString(R.string.coldBeachName));
			tvCountry.setText(getString(R.string.coldBeachCountry));
			tvSnippet.setText(getString(R.string.coldBeachSnippet));
			editor.putFloat("lat", Float.parseFloat(getString(R.string.coldBeachLat)));
			editor.putFloat("long", Float.parseFloat(getString(R.string.coldBeachLong)));
			editor.putString("park", getString(R.string.coldBeachName));
		}
		else if (terrain.equals("Beach") && temperature.equals("Mild")) {
			tvName.setText(getString(R.string.mildBeachName));
			tvCountry.setText(getString(R.string.mildBeachCountry));
			tvSnippet.setText(getString(R.string.mildBeachSnippet));
			editor.putFloat("lat", Float.parseFloat(getString(R.string.mildBeachLat)));
			editor.putFloat("long", Float.parseFloat(getString(R.string.mildBeachLong)));
			editor.putString("park", getString(R.string.mildBeachName));
		}
		else if (terrain.equals("Beach") && temperature.equals("Hot")) {
			tvName.setText(getString(R.string.hotBeachName));
			tvCountry.setText(getString(R.string.hotBeachCountry));
			tvSnippet.setText(getString(R.string.hotBeachSnippet));
			editor.putFloat("lat", Float.parseFloat(getString(R.string.hotBeachLat)));
			editor.putFloat("long", Float.parseFloat(getString(R.string.hotBeachLong)));
			editor.putString("park", getString(R.string.hotBeachName));
		}
		
		editor.apply();
	}
}
